from odoo import fields, models


class ActWindowViewnani(models.Model):
    _inherit = 'ir.actions.act_window.view'

    view_mode = fields.Selection(
        selection_add=[('custom_gantt', "Custom Gantt")], ondelete={'custom_gantt': 'cascade'})
